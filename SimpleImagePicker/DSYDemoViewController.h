//
//  DSYDemoViewController.h
//  SimpleImagePicker
//
//  Created by Matias Castro on 18-07-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SimpleImagePicker/SimpleImagePicker.h"

@interface DSYDemoViewController : UIViewController <SimpleImagePickerProtocol>

@end
