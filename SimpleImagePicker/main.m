//
//  main.m
//  SimpleImagePicker
//
//  Created by Matias Castro on 18-07-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DSYAppDelegate class]));
    }
}
