//
//  DSYDemoViewController.m
//  SimpleImagePicker
//
//  Created by Matias Castro on 18-07-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import "DSYDemoViewController.h"

@interface DSYDemoViewController ()
@property IBOutlet UIImageView *imageView;
@property SimpleImagePicker *picker;
@end

@implementation DSYDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pickImage:(id)sender{
    self.picker = [SimpleImagePicker new];
    self.picker.delegate = self;
    [self.picker pickerForViewController:self AndExtraButtonTitles:@[@"Borrar imagen", @"Ver imagen"]];
}

-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFailWithError:(NSError *)error{
    
}

-(void)SimpleImagePickerDidCancelWithPicker:(SimpleImagePicker *)picker{
    
}

-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFinishWithImage:(UIImage *)image andURL:(NSURL *)url{
    self.imageView.image = image;
}

-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFinishWithImage:(UIImage *)image{
    self.imageView.image = image;
}

-(void)SimpleImagePicker:(SimpleImagePicker *)picker didPickExtraTitleAtIndex:(NSInteger)index{
    NSLog(@"extra index %i", (int)index);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
