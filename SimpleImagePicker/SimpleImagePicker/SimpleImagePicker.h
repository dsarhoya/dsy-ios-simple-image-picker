//
//  SimpleImagePicker.h
//  EpiAutoInjector
//
//  Created by Matias Castro on 17-07-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SimpleImagePicker;
@protocol SimpleImagePickerProtocol <NSObject>
-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFinishWithImage:(UIImage *)image andURL:(NSURL *)url;
-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFinishWithImageFromCamera:(UIImage *)image;
-(void)SimpleImagePickerDidCancelWithPicker:(SimpleImagePicker *)picker;
-(void)SimpleImagePicker:(SimpleImagePicker *)picker didFailWithError:(NSError *)error;
@optional
-(void)SimpleImagePicker:(SimpleImagePicker *)picker didPickExtraTitleAtIndex:(NSInteger)index;
@end

@interface SimpleImagePicker : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
}

@property (nonatomic, weak) id<SimpleImagePickerProtocol> delegate;

-(void)pickerForViewController:(UIViewController *)vc;
-(void)pickerForViewController:(UIViewController *)vc AndExtraButtonTitles:(NSArray *)extraTitles;

@end
