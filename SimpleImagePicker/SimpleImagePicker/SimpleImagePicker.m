//
//  SimpleImagePicker.m
//  EpiAutoInjector
//
//  Created by Matias Castro on 17-07-14.
//  Copyright (c) 2014 Dsarhoya. All rights reserved.
//

#import "SimpleImagePicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <DSYUtils/UIImage+addedFunctionalities.h>

#ifndef SimpleImagePickerLocalizedStrings
#define SimpleImagePickerLocalizedStrings(key) \
[self.langBundle localizedStringForKey:key value:nil table:SimpleImagePickerStringsName]
#endif

/** The name of the bundle for the iOS plateform. */
static NSString * const SimpleImagePickerBundleName = @"SimpleImagePicker-iOS";
/** The table name for the relative time strings. */
static NSString * const SimpleImagePickerStringsName = @"SimpleImagePickerLocalizable";

@interface SimpleImagePicker (){
    
}

@property (nonatomic, weak) UIViewController *vc;
@property NSArray *titles;
@property NSBundle *langBundle;
@property NSLocale *locale;
@end

@implementation SimpleImagePicker{
    
}

-(id)init{
    self = [super init];
    if (self) {
        _titles = @[];
        _locale    = [NSLocale localeWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        _langBundle = [self langBundleForLocaleWithIdentifier:_locale.localeIdentifier];
    }
    return self;
}

-(void)pickerForViewController:(UIViewController *)vc{
    return [self pickerForViewController:vc AndExtraButtonTitles:@[]];
}

-(void)pickerForViewController:(UIViewController *)vc AndExtraButtonTitles:(NSArray *)extraTitles{
    _vc = vc;
    _titles = extraTitles;
    
    UIAlertController * view = [UIAlertController alertControllerWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_PICK_FROM_TITLE")
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                         actionWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_PICK_FROM_CAMERA")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                             {
                                 [self imageFromCamera];
                         }];
    
    UIAlertAction* gallery = [UIAlertAction
                         actionWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_PICK_FROM_GALLERY")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self imageFromLibrary];
                         }];
    
    [view addAction:camera];
    [view addAction:gallery];
    
    int i = 0;
    for (NSString *title in self.titles) {
        UIAlertAction* extra = [UIAlertAction
                                  actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      if ([self.delegate respondsToSelector:@selector(SimpleImagePicker:didPickExtraTitleAtIndex:)]) {
                                          [self.delegate SimpleImagePicker:self didPickExtraTitleAtIndex:i];
                                      }
                                  }];
        [view addAction:extra];
        i++;
    }
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_PICK_FROM_CANCEL")
                             style:(UIAlertActionStyleCancel)
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    [view addAction:cancel];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        if (@available(iOS 8.0, *)) {
            [view popoverPresentationController].sourceView = self.vc.view;
            [view popoverPresentationController].sourceRect = CGRectMake(self.vc.view.bounds.size.width / 2.0, self.vc.view.bounds.size.height / 2.0, 1.0, 1.0);
            [view.popoverPresentationController setPermittedArrowDirections:0];
        }
    }
    
    [self.vc presentViewController:view animated:YES completion:nil];
}

-(void)imageFromCamera{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        if ([mediaTypes containsObject:(NSString *)kUTTypeImage])
        {
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
            cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
            cameraUI.allowsEditing = YES;
            
            cameraUI.delegate = self;
            [self.vc presentViewController:cameraUI animated:YES completion:^{
                //
            }];
        }
        else {
            UIAlertView *alertB = [[UIAlertView alloc] initWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_TITLE") message:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_UNKNOWN") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertB show];
        }
    }
    else {
        UIAlertView *alertA = [[UIAlertView alloc] initWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_TITLE") message:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_NO_CAMERA") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertA show];
    }
}

-(void)imageFromLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        if ([mediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
//            cameraUI.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
//            cameraUI.videoQuality = UIImagePickerControllerQualityTypeHigh;
            cameraUI.delegate = self;
            [self.vc presentViewController:cameraUI animated:YES completion:^{
                //
            }];
        }
        else {
            UIAlertView *alertB = [[UIAlertView alloc] initWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_TITLE") message:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_UNKNOWN") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertB show];
        }
    }
    else {
        UIAlertView *alertA = [[UIAlertView alloc] initWithTitle:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_TITLE") message:SimpleImagePickerLocalizedStrings(@"ACTION_ERROR_NO_LIBRARY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertA show];
    }
}


- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self.vc dismissViewControllerAnimated:YES
                             completion:^{
                                 [self.delegate SimpleImagePickerDidCancelWithPicker:self];
                             }];
}

- (void) imagePickerController:(UIImagePickerController *) picker
 didFinishPickingMediaWithInfo:(NSDictionary *) info {
    
    if (info[UIImagePickerControllerReferenceURL]) {
        UIImage *selectedImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        selectedImage = [selectedImage fixOrientation];
        [self.vc dismissViewControllerAnimated:YES
                                    completion:^{
                                        [self.delegate SimpleImagePicker:self didFinishWithImage:selectedImage andURL:info[UIImagePickerControllerReferenceURL]];
                                    }];
    }else{
        UIImage *selectedImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
        selectedImage = [selectedImage fixOrientation];
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
            [self.vc dismissViewControllerAnimated:YES
                                        completion:^{
                                            [self.delegate SimpleImagePicker:self didFinishWithImageFromCamera:selectedImage];
                                        }];
            return;
        }
        
        [library writeImageToSavedPhotosAlbum:[selectedImage CGImage] orientation:(ALAssetOrientation)[selectedImage imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
            if (error) {
                [self.vc dismissViewControllerAnimated:YES
                                            completion:^{
                                                [self.delegate SimpleImagePicker:self didFailWithError:error];
                                            }];
            } else {
                [self.vc dismissViewControllerAnimated:YES
                                            completion:^{
                                                [self.delegate SimpleImagePicker:self didFinishWithImage:selectedImage andURL:assetURL];
                                            }];
            }  
        }];
    }
}

#pragma mark - languages

- (NSBundle *)langBundleForLocaleWithIdentifier:(NSString *)localeIdentifier{
    
    static NSBundle *classBundle  = nil;
    static NSBundle *momentBundle = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        classBundle = [NSBundle bundleForClass:[self class]];
        NSString *bundlePath = [classBundle pathForResource:SimpleImagePickerBundleName ofType:@"bundle"];
        momentBundle = [NSBundle bundleWithPath:bundlePath];
    });
    
    NSBundle *bundle   = momentBundle ?: classBundle;
    NSString *lang     = [localeIdentifier substringToIndex:2];
    NSString *langPath = [bundle pathForResource:lang ofType:@"lproj"];
    
    if (langPath)
    {
        return [NSBundle bundleWithPath:langPath];
    } else
    {
        NSArray *preferredLocales = [NSLocale preferredLanguages];
        
        for (NSString *preferredLocale in preferredLocales)
        {
            langPath = [bundle pathForResource:preferredLocale ofType:@"lproj"];
            
            if (langPath)
            {
                return [NSBundle bundleWithPath:langPath];
            }
        }
    }
    return nil;
}
@end
